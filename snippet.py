#!/Users/gotonorio/.pyenv/shims/python
# 簡易なterminal用snippet by N.goto
import argparse
import csv
import pyperclip


def main() -> None:
    parser = argparse.ArgumentParser()
    # add_argumentで指定したオプションは必須。
    parser.add_argument("key_command", help="snippet用キーコマンドが必要です。")
    args = parser.parse_args()
    key_cmd = args.key_command

    d, e = data_read('/Users/gotonorio/bin/snippet.dat', key_cmd)

    if d:
        pyperclip.copy(d[key_cmd])
        print('please paste')
    else:
        print(key_cmd + ' は登録されていません。')
        print(e)


def data_read(datafile, key_cmd):
    try:
        with open(datafile, "r") as csv_file:
            reader = csv.DictReader(csv_file)
            cmd_dict = {}
            cmd_error = []
            for row in reader:
                if row['key'] == key_cmd:
                    cmd_dict[row['key']] = row['command']
                    return cmd_dict, cmd_error
                cmd_error.append(row['key'])
            return cmd_dict, cmd_error
    except FileNotFoundError as e:
        print("snippet.datファイルが見つかりません", e)
    except Exception as e:
        print(e)


def data_write(datafile, dict):
    """ 使用しない """
    with open(datafile, 'w') as csv_file:
        fieldnames = ['key', 'command']
        writer = csv.DictWriter(csv_file, fieldnames=fieldnames)
        writer.writeheader()
        for k, v in dict.items():
            writer.writerow({'key': k, 'command': v})


if __name__ == "__main__":
    main()
